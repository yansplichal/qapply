# README #

This is a pretty standard R package...  The repository is split into the archive,`pkg` and `working`, which contains the working structure of the current release.  

Active testing is done on Ubuntu-12.04 and SGE6.

Like the familiar `lapply` function, `qapply` applies a function to each element of the first argument and returns a list; it does so via grid engine, however, allowing for simple implementation of embarrassingly parallel execution.

In general, you will:
`qapply( <Vector>, FUN, numCores=N)`
## Default behavior for packet building is to empty the queue
.nPerPacket <- function(nr, nc){
  standard <- floor(nr/nc)
  nBump <- nr-nc*standard 
  rep(standard, nc) + c(rep(1, nBump), rep(0, nc-nBump))
}

.getResources <- function(){
  system("  qconf -sep | awk '/SUM/ { print $2 }' ", intern=TRUE )
}
